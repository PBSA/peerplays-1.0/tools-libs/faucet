# Faucet

`Faucet` is a python-based faucet for PeerPlays.

## Usage
There are two APIs
* `'/api/v1/accounts', methods=['POST'], defaults={'referrer': None}` # This is the legacy API
* `'/api/v2/accounts', methods=['POST'], defaults={'referrer': None}` # This is the asynchronous API

v2 moves account creation to a rq (redis queue) worker process, so that a quick response is offered by the API.
This helps in managing peak loads, that is simultaneous account creations.
Depending on the transaction state, the following states are returned

1. init
2. run
3. acccount exists

## Installation

```
sudo apt install python3-virtualenv libffi-dev libssl-dev python-dev python3-pip python3-venv git redis
git clone https://gitlab.com/PBSA/tools-libs/faucet.git && cd faucet
virtualenv -p python3 env
source env/bin/activate
pip install -r requirements.txt # to install dependencies
cp config-example.yml config.yml
edit config.yml and provide private keys and settings
python manage.py install
```

## Use virtual environment if required, to minimize libaray conflicts

## Usage

* Start the worker dispatcher for v2 asynchronous API:
  * `python work.py &` # for starting workers (supports v2 API)
* Then start the faucet in either normal or debug mode with one of the following:
  * `python manage.py run` # for normal run
  * `python manage.py start` # for debug

Note: if you omit starting `work.py`, then v2 API requests will queue but accounts will NOT be created until `work.py` is started.

The faucet is then available at URL `http://localhost:5000`

## Test

### With Docker:

* ```docker build -t faucet-test .``` # Build the container
* ```docker run -p 5000:5000 faucet-test``` # Start the container
* ```python3 -m unittest tests/test_faucet_v1.py``` # Start v1 API test
* ```python3 -m unittest tests/test_faucet_v2.py``` # Start v2 API test
* ```docker stop``` # Stop the container

### Without Docker:

* In a terminal or GNU screen session:
  * `python work.py &`
  * `python manage.py run`
* In another terminal:
  * ```python3 -m unittest tests/test_faucet_v1.py``` # Start v1 API test
  * ```python3 -m unittest tests/test_faucet_v2.py``` # Start v2 API test

### Manual testing with curl:

* In a terminal or GNU screen session:
  * `python work.py &`
  * `python manage.py run`
* In another terminal:
  * `curl http://localhost:5000/api/v1/accounts -H 'Content-Type: application/json' -d '{"account":{"name":"account-name","owner_key":"TEST6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV","active_key":"TEST6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV","memo_key":"TEST6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV"}}'`
  * `curl http://localhost:5000/api/v2/accounts -H 'Content-Type: application/json' -d '{"account":{"name":"account-name","owner_key":"TEST6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV","active_key":"TEST6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV","memo_key":"TEST6MRyAjQq8ud7hVNYcfnVPJqcVpscN5So8BhtHuGYqET5GDW5CV"}}'`
  * (v1 and v2 API's, respectively)

## Nginx configuration (optional)

NGINX can be used to isolate the faucet behind a reverse proxy and/or to secure the faucet API with TLS/SSL.

Run `uwsgi --ini wsgi.ini`

and use a configuration similar tothis

```
user peerplays;
worker_processes  4;

events {
    worker_connections  2048;
}

http {
    include       mime.types;
    default_type  application/octet-stream;
    access_log  /www/logs/access.log;
    error_log  /www/logs/error.log;
    log_not_found off;
    sendfile        on;
    keepalive_timeout  65;
    gzip  on;

    upstream websockets {
      server localhost:9090;
      server localhost:9091;
    }

    server {
        listen       80;
        if ($scheme != "https") {
                return 301 https://$host$request_uri;
        }

        listen       443 ssl;
        server_name  peerplays-wallet.com;
        ssl_certificate      /etc/nginx/ssl/peerplays-wallet.com.crt;
        ssl_certificate_key /etc/nginx/ssl/peerplays-wallet.com.key;
        ssl_session_cache    shared:SSL:1m;
        ssl_session_timeout  5m;
        ssl_ciphers  HIGH:!aNULL:!MD5;
        ssl_prefer_server_ciphers  on;

        location ~ /ws/? {
            access_log on;
            proxy_pass http://websockets;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header Host $host;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
            proxy_next_upstream     error timeout invalid_header http_500;
            proxy_connect_timeout   2;
        }
        location ~ ^/[\w\d\.-]+\.(js|css|dat|png|json)$ {
            root /www/wallet;
            try_files $uri /wallet$uri =404;
        }
        location / {
            root /www/wallet;
        }
        location /api {
                include uwsgi_params;
                uwsgi_pass unix:/tmp/faucet.sock;
        }

    }
}
```
